package com.factory;

import com.constant.GildedRoseConst;
import com.gildedrose.Item;
import com.products.*;

public class ProductFactory {
    public Product getProduct(Item item) {
            switch (item.name) {
                case GildedRoseConst.CONST_AGED_BRIE:
                    return new AgedBrie(item);
                case GildedRoseConst.CONST_BACKSTAGE_PASSES:
                    return new BackstagePasses(item);
                case GildedRoseConst.CONST_SULFURAS:
                    return new Sulfuras(item);
                case GildedRoseConst.CONST_CONJURED:
                    return new Conjured(item);
                default:
                    return new CustomProduct(item);
            }
    }
}
