package com.quality;

import com.constant.GildedRoseConst;

public class Days {
    private int sellIn = 0;
    public Days(int sellIn){
            this.sellIn = sellIn;
    }
    public void updateSellIn(int variable){
        this.sellIn = this.sellIn + variable;
    }
    public boolean sellInIsNegative(){
        return this.sellIn < GildedRoseConst.CONST_MINIMAL_SELLIN;
    }
    public boolean sellInIsPositive(){
        return this.sellIn > GildedRoseConst.CONST_MINIMAL_SELLIN;
    }
    public boolean sellInIsLowerThanEleven(){
        return this.sellIn < GildedRoseConst.CONST_ELEVEN;
    }
    public boolean sellInIsLowerThanSix(){
        return this.sellIn < GildedRoseConst.CONST_SIX;
    }
    public int getSellIn(){
        return this.sellIn;
    }
    public void qualityDropsToZero(){
        this.sellIn = GildedRoseConst.CONST_MINIMAL_SELLIN;
    }
}
