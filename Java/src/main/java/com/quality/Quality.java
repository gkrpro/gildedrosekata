package com.quality;

import com.constant.GildedRoseConst;

public class Quality {
    private int quality = 0;
    public Quality(int quality){
        if(qualityIsNegative(quality)){
            this.quality = GildedRoseConst.CONST_MINIMAL_QUALITY;
        }else if(qualityIsHigherThanFifty(quality)){
            this.quality = GildedRoseConst.CONST_MAXIMAL_QUALITY;
        }else{
            this.quality = quality;
        }
    }
    public void updateQuality(int variable){
        this.quality = this.quality + variable;
    }
    public boolean qualityIsNegative(){
        return this.quality < GildedRoseConst.CONST_MINIMAL_QUALITY;
    }
    public boolean qualityIsNegative(int quality){
        return quality < GildedRoseConst.CONST_MINIMAL_QUALITY;
    }
    public boolean qualityIsPositive(){
        return this.quality > GildedRoseConst.CONST_MINIMAL_QUALITY;
    }
    public boolean qualityIsHigherThanFifty(){
        return this.quality > GildedRoseConst.CONST_MAXIMAL_QUALITY;
    }
    public boolean qualityIsHigherThanFifty(int quality){
        return quality > GildedRoseConst.CONST_MAXIMAL_QUALITY;
    }
    public boolean qualityIsLowerThanFifty(){
        return quality < GildedRoseConst.CONST_MAXIMAL_QUALITY;
    }
    public void qualityDropsToZero(){
        this.quality = GildedRoseConst.CONST_MINIMAL_QUALITY;
    }
    public int getQuality(){
        return this.quality;
    }
}
