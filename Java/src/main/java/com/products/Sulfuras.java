package com.products;

import com.constant.GildedRoseConst;
import com.gildedrose.Item;

public class Sulfuras implements Product{
    private Item item = null;

    public Sulfuras(Item item){
        this.item = item;
    }

    @Override
    public void updateQuality() {
        this.item.quality = GildedRoseConst.CONST_LEGENDARY_QUALITY;
        this.item.sellIn = GildedRoseConst.CONST_LEGENDARY_SELLIN;
    }

    @Override
    public Item getItem() {
        return this.item;
    }
}
