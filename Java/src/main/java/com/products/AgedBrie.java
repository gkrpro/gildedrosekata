package com.products;

import com.constant.GildedRoseConst;
import com.gildedrose.Item;
import com.quality.Days;
import com.quality.Quality;

public class AgedBrie implements Product{
    private Item item = null;
    private Quality qualityObject;
    private Days daysObject;

    public AgedBrie(Item item){
        this.item = item;
        this.qualityObject = new Quality(this.item.quality);
        this.daysObject = new Days(this.item.sellIn);
    }

    @Override
    public void updateQuality() {
        if(this.qualityObject.qualityIsLowerThanFifty()){
            this.qualityObject.updateQuality(GildedRoseConst.CONST_MINIMAL_VARIATION);
        }
        this.daysObject.updateSellIn(- GildedRoseConst.CONST_MINIMAL_VARIATION);
        if(this.daysObject.sellInIsNegative() && this.qualityObject.qualityIsPositive()){
            this.qualityObject.updateQuality(GildedRoseConst.CONST_MINIMAL_VARIATION);
        }
        this.item.quality = this.qualityObject.getQuality();
        this.item.sellIn = this.daysObject.getSellIn();
    }

    @Override
    public Item getItem() {
        return this.item;
    }
}
