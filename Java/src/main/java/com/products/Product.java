package com.products;

import com.gildedrose.Item;

public interface Product {
    void updateQuality();
    Item getItem();
    String toString();
}
