package com.constant;

public final class GildedRoseConst {
    public static final int CONST_MINIMAL_QUALITY = 0;
    public static  final int CONST_MAXIMAL_QUALITY = 50;
    public static  final int CONST_LEGENDARY_QUALITY = 80;
    public static  final int CONST_LEGENDARY_SELLIN = 0;
    public static  final int CONST_MINIMAL_VARIATION = 1;
    public static  final int CONST_MAXIMAL_VARIATION = 2;
    public static final int CONST_MINIMAL_SELLIN = 0;
    public static final int CONST_SIX = 6;
    public static final int CONST_ELEVEN = 11;
    public static  final String CONST_BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static  final String CONST_AGED_BRIE = "Aged Brie";
    public static  final String CONST_SULFURAS = "Sulfuras, Hand of Ragnaros";
    public static  final String CONST_CONJURED = "Conjured";
}
