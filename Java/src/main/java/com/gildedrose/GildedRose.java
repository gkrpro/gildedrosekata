package com.gildedrose;

import com.factory.ProductFactory;
import com.products.Product;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality(){
        ProductFactory productFactory = new ProductFactory();
        for (Item item : items){
            Product product = productFactory.getProduct(item);
            product.updateQuality();
            item = product.getItem();
        }
    }
}