Feature: GildedRose BDD Test.
  Scenario: Aged Brie actually increases in Quality the older it gets.
    Given I enter Aged Brie as item name.
    When I update the quality.
    Then Quality should be updated and increased