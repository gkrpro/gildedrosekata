Feature: GildedRose BDD Test.
  Scenario: Quality increases by 2 when there are 10 days or less.
    Given I enter Aged Brie or Backstage passes or else but not Sulfuras as item name.
    And Sell date is 10 days or less.
    When I update the quality.
    Then Quality should be updated and increased by 5.