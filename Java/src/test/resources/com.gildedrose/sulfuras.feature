Feature: GildedRose BDD Test.
  Scenario: Sulfuras being a legendary item, never has to be sold or decreases in Quality.
    Given I enter Sulfuras as item name.
    When I update the quality.
    Then Quality should never decrease and never has to be sold.