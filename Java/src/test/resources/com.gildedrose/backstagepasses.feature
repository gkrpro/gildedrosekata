Feature: GildedRose BDD Test.
  Scenario: Backstage passes like aged brie, increases in Quality as its SellIn value approaches.
    Given I enter Backstage passes as item name.
    When I update the quality.
    Then Quality should be increased
    And  SellIn should be decreased.