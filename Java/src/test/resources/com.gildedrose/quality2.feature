Feature: GildedRose BDD Test.
  Scenario: Quality increases by 3 when there are 5 days or less but Quality drops to 0 after the concert.
    Given I enter Aged Brie or Backstage passes or else but not Sulfuras as item name.
    And Sell date is 5 days or less.
    When I update the quality.
    Then Quality should be updated and increased by 2.