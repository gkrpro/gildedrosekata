package com.gildedrose;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

public class GildedRoseStepdefs {
    Item[] items = null;
    GildedRose app = null;
    @Given("I enter Aged Brie as item name.")
    public void iEnterAgedBrieAsItemName() {
        items = new Item[] { new Item("Aged Brie", 30, 30),
                new Item("Aged Brie", 20, 30)  };
        app = new GildedRose(items);
    }

    @When("I update the quality.")
    public void iUpdateTheQuality() {
        app.updateQuality();
    }

    @Then("Quality should be updated and increased")
    public void qualityShouldBeUpdatedAndIncreased() {
        assertEquals(31, app.items[0].quality);
        assertEquals(31, app.items[1].quality);
    }
    @Given("I enter Backstage passes as item name.")
    public void iEnterBackstagePassesAsItemName() {
        items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 30, 10),
                new Item("Backstage passes to a TAFKAL80ETC concert", 20, 10) };
        app = new GildedRose(items);
    }

    @And("SellIn should be increased.")
    public void sellinShouldBeIncreased() {
        assertEquals(29, app.items[0].sellIn);
        assertEquals(19, app.items[1].sellIn);
    }

    @Then("Quality should be increased")
    public void qualityShouldBeIncreased() {
        assertEquals(11, app.items[0].quality);
        assertEquals(11, app.items[1].quality);
    }
    @Given("I enter Aged Brie or Backstage passes or else but not Sulfuras as item name.")
    public void iEnterAgedBrieOrBackstagePassesOrElseButNotSulfurasAsItemName() {
        items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 9, 10),
                new Item("Backstage passes to a TAFKAL80ETC concert", 3, 5),
                new Item("Backstage passes to a TAFKAL80ETC concert", -1, 5) };
        app = new GildedRose(items);
    }

    @And("Sell date is {int} days or less.")
    public void sellDateIsDaysOrLess(int arg0) {
        assertEquals(9, app.items[0].sellIn);
        assertEquals(3, app.items[1].sellIn);
        assertEquals(-1, app.items[2].sellIn);
    }

    @Then("Quality should be updated and increased by {int}.")
    public void qualityShouldBeUpdatedAndIncreasedBy(int arg0) {
        assertEquals(12, app.items[0].quality);
        assertEquals(8, app.items[1].quality);
        assertEquals(0, app.items[2].quality);

    }
    @Given("I enter Sulfuras as item name.")
    public void iEnterSulfurasAsItemName() {
        items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 0, 80) };
        app = new GildedRose(items);
    }

    @Then("Quality should never decrease and never has to be sold.")
    public void qualityShouldNeverDecreaseAndNeverHasToBeSold() {
        assertEquals(80, app.items[0].quality);
    }

   @Given("I enter Conjured as item name.")
    public void iEnterConjuredAsItemName() {
        items = new Item[] { new Item("Conjured", 30, 30),
                new Item("Conjured", 20, 19) ,
                new Item("Conjured", 10, 10) ,
                new Item("Conjured", 5, 5),
                new Item("Conjured", 0, 3) };
        app = new GildedRose(items);
    }

    @Then("Quality should be updated and decreased")
    public void qualityShouldBeUpdatedAndDecreased() {
        assertEquals(28, app.items[0].quality);
        assertEquals(17, app.items[1].quality);
        assertEquals(8, app.items[2].quality);
        assertEquals(3, app.items[3].quality);
        assertEquals(1, app.items[4].quality);
    }

    @Then("SellIn should be decreased.")
    public void sell_in_should_be_decreased() {
        assertEquals(11, app.items[0].quality);
    }
}
