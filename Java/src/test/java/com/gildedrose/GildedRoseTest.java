package com.gildedrose;

import com.constant.GildedRoseConst;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class GildedRoseTest {
    @Test
    void gildedRoseConstructor(){
        Item[] items = {new Item(GildedRoseConst.CONST_BACKSTAGE_PASSES, 15, 30)};
        Item[] expectedItems = {new Item(GildedRoseConst.CONST_BACKSTAGE_PASSES, 15, 30)};
        assertAllItem(items, expectedItems);
    }

    private void assertAllItem(Item[] items, Item[] expectedItems) {
        assertExpectedQuality(items, expectedItems);
        assertExpectedSellIn(items, expectedItems);
        assertExpectedName(items, expectedItems);
    }

    //Backstage Passes
    @Test
    void backstagePassesDecreasesInSellInTheOlderItGets(){
        Item[] items = new Item[] { new Item(GildedRoseConst.CONST_BACKSTAGE_PASSES, 15, 3)};
        int expected = 14;
        assertExpectedSellIn(items, expected);

    }

    @Test
    void isBackStagePassSellInNegative(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_BACKSTAGE_PASSES, -1, 30)
        };
        int expected = 0;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityIsPositiveBackstagePasses(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_BACKSTAGE_PASSES, 1, 30)
        };
        int expected = 33;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityIsPositiveBackstagePassesSellInLowerThan6(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_BACKSTAGE_PASSES, 5, 30)
        };
        int expected = 33;
        assertExpectedQuality(items, expected);
    }

    @Test
    void qualityIsHigherThan11SellInPositiveBackstagePasses(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_AGED_BRIE, 17, 30)
        };
        int expected = 31;
        assertExpectedQuality(items, expected);
    }
    //Aged Brie
    @Test
    void qualityIsLessThan6SellPositiveInAgedBrie(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_AGED_BRIE, 3, 30)
        };
        int expected = 31;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityIsPositiveAgedBrie(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_AGED_BRIE, 1, 30)
        };
        int expected = 31;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityIsPositiveSellInNegativeAgedBrie(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_AGED_BRIE, -1, 30)
        };
        int expected = 32;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityIsLessThan11SellInPositiveAgedBrie(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_AGED_BRIE, 7, 30)
        };
        int expected = 31;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityIsHigherThan11SellInPositiveAgedBrie(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_AGED_BRIE, 17, 30)
        };
        int expected = 31;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityOfAnAgedBrieIsNeverNegative(){
        Item[] items = new Item[] { new Item(GildedRoseConst.CONST_AGED_BRIE, 0, -3)};
        int expected = 0;
        assertNotExpectedQuality(items, expected);

    }
    @Test
    void qualityOfAnAgedBrieIsPositiveAndHigherThan50(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_AGED_BRIE, 5, 55)
        };
        int expected = 50;
        assertExpectedQuality(items, expected);
    }

    //Sulfuras
   /* @Test
    void isSulfurasSellInNegative(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_SULFURAS, -1, 30)
        };
        int expected = 30;
        assertExpectedQuality(items, expected);
    }*/
    @Test
    void sulfurasNeverSoldOrDecreasesInQuality(){
        Item[] items = new Item[] { new Item(GildedRoseConst.CONST_SULFURAS, 30, 80)};
        int expected = 80;
        assertExpectedQuality(items, expected);
    }
    @Test
    void isNotAgedBrieNorBackStagePassAndQualityIsPositive(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_SULFURAS, 1, 30)
        };
        int expected = 30;
        assertNotExpectedQuality(items, expected);
    }
    @Test
    void qualityIsPositiveSulfuras(){
        Item[] items = new Item[] {
                new Item(GildedRoseConst.CONST_SULFURAS, 1, 30)
        };
        int expected = 30;
        assertNotExpectedQuality(items, expected);
    }

    //Conjured
    @Test
    void customNameSellInNegativeAndQualityPositive(){
        Item[] items = new Item[] { new Item("Unknown", -1, 30)};
        int expected = 28;
        assertExpectedQuality(items, expected);
    }
    @Test
    void customNameSellInNegative(){
        Item[] items = new Item[] { new Item("Conjured", -1, 49)};
        int expected = 45;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityOfBackstagePassesPositiveAndNegativeSellIn(){
        Item[] items = new Item[] {
                new Item("Conjured", -1, 30)
        };
        int expected = 26;
        assertExpectedQuality(items, expected);
    }

    //Unknown
    @Test
    void isNotAgedBrieAndNotBackstagePassesAndSulfurasAndQualityPositive(){
        Item[] items = new Item[] {
                new Item("Unknown", 3, 30)
        };
        int expected = 29;
        assertExpectedQuality(items, expected);
    }
    @Test
    void isNotAgedBrieAndNotBackstagePassesAndSulfurasAndSellInNegative(){
        Item[] items = new Item[] {
                new Item("Unknown", -3, 30)
        };
        int expected = 28;
        assertExpectedQuality(items, expected);
    }
    @Test
    void  sellInNegativeisNotAgedBrieAndNotBackstagePassesQualityPositiveAndNotSulfuras(){
        Item[] items = new Item[] { new Item("Unknown", -1, 3)};
        int expected = 1;
        assertExpectedQuality(items, expected);

    }
    @Test
    void qualityIsPositiveUnknownItem(){
        Item[] items = new Item[] {
                new Item("Unknown", 1, 30)
        };
        int expected = 29;
        assertExpectedQuality(items, expected);
    }
    @Test
    void qualityIsPositiveSellInLowerThan11UnknownItem(){
        Item[] items = new Item[] {
                new Item("Unknown", 10, 30)
        };
        int expected = 29;
        assertExpectedQuality(items, expected);
    }


    //Assert methods
    void assertExpectedQuality(Item[] items, int expected){
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(expected, app.items[0].quality);
    }
    void assertNotExpectedQuality(Item[] items, int expected){
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertNotEquals(expected, app.items[0].quality);
    }
    void assertExpectedSellIn(Item[] items, int expected){
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals(expected, app.items[0].sellIn);
    }
    void assertExpectedName(GildedRose actual, GildedRose expected){
        actual.updateQuality();
        expected.updateQuality();
        assertEquals(expected.items[0].name, actual.items[0].name);
    }
    void assertExpectedQuality(Item[] items, Item[] expectedItems){
        GildedRose app = new GildedRose(items);
        GildedRose test = new GildedRose(expectedItems);
        app.updateQuality();
        test.updateQuality();
        assertEquals(test.items[0].quality, app.items[0].quality);
    }
    void assertExpectedSellIn(Item[] items, Item[] expectedItems){
        GildedRose app = new GildedRose(items);
        GildedRose test = new GildedRose(expectedItems);
        app.updateQuality();
        test.updateQuality();
        assertEquals(test.items[0].sellIn, app.items[0].sellIn);
    }
    void assertExpectedName(Item[] items, Item[] expectedItems){
        GildedRose app = new GildedRose(items);
        GildedRose test = new GildedRose(expectedItems);
        app.updateQuality();
        test.updateQuality();
        assertEquals(test.items[0].name, app.items[0].name);
    }
}