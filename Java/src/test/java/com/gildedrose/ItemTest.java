package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void testToString() {
        Item item = new Item("TestName", 5, 50);
        assertEquals("TestName, 5, 50", item.toString());
    }
}